package com.fpt.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fpt.dal.IncidentDAOImpl;

@Configuration
@ComponentScan(basePackages="com.fpt")
@EnableWebMvc
public class MvcConfiguration {
	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		dataSource.setUrl("jdbc:sqlserver://localhost:1433;databaseName= TicketManagement");
		dataSource.setUsername("sa");
		dataSource.setPassword("123");

		return dataSource;
	}
	
	@Bean
	public IncidentDAOImpl getHuDAO() {
		return new IncidentDAOImpl(getDataSource());
	}
}
