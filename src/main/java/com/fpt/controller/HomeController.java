package com.fpt.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fpt.config.SpringSecurityUtil;
import com.fpt.dal.IncidentDAO;
import com.fpt.model.Impact;
import com.fpt.model.Incident;
import com.fpt.model.Organization;
import com.fpt.model.Origin;
import com.fpt.model.Priority;
import com.fpt.model.Requestor;
import com.fpt.model.Status;
import com.fpt.model.Type;
import com.fpt.model.Urgency;
import com.fpt.model.User;

@Controller
public class HomeController {
	@Autowired
	private IncidentDAO incidentDAO;

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView welcomePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("admin/home");
		return model;
	}

	@RequestMapping(value = { "/homepage" }, method = RequestMethod.GET)
	public ModelAndView huy() {
		ModelAndView model = new ModelAndView();
		model.setViewName("admin/home");
		return model;
	}

	@RequestMapping(value = "/Login", method = RequestMethod.GET)
	public ModelAndView loginPage(@RequestParam(value = "error", required = false) String error) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid Credentials provided.");
		}

		model.setViewName("Login");
		return model;
	}

	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public ModelAndView homePage() {
		ModelAndView mav = new ModelAndView("admin/home");
		return mav;
	}

	@RequestMapping(value = "/admin/newincident", method = RequestMethod.GET)
	public ModelAndView IncidentPage(@RequestParam(value = "id", required = false) String id) {
		ModelAndView mav = new ModelAndView("admin/newincident");
		List<Organization> listOrganization = incidentDAO.getAllOrganization();
		List<Requestor> listRequestor = incidentDAO.getAllRequestor();
		List<Origin> listOrigin = incidentDAO.getAllOrigin();
		List<Impact> listImpact = incidentDAO.getAllImpact();
		List<Urgency> listUrgency = incidentDAO.getAllUrgency();
		List<Type> listType = incidentDAO.getAllType();
		List<User> listUser = incidentDAO.getAllUser();
		mav.addObject("listUser", listUser);
		mav.addObject("listType", listType);
		mav.addObject("listOrganization", listOrganization);
		mav.addObject("listRequestor", listRequestor);
		mav.addObject("listOrigin", listOrigin);
		mav.addObject("listImpact", listImpact);
		mav.addObject("listUrgency", listUrgency);
		if (SpringSecurityUtil.hasRole("ROLE_ADMIN")) {
			mav.addObject("enable", true);
		}else {
			mav.addObject("enable", false);
		}
		if (id != null) {
			int idx = Integer.parseInt(id);
			mav.addObject("incident", incidentDAO.getIncidentById(idx));
		} else {
			mav.addObject("incident", new Incident());
		}
		return mav;
	}
	
	@RequestMapping(value = "/admin/solveincident", method = RequestMethod.GET)
	public String solveIncident(@RequestParam(value = "incidentId", required = false) int incidentId,
			@RequestParam(value = "statusId", required = false) int statusId) {
		incidentDAO.updateStatusIncident(incidentId, statusId);
		return "redirect:myincident";
	}

	@RequestMapping(value = "/admin/newincident", method = RequestMethod.POST)
	public String IncidentPage(@Validated @ModelAttribute("incident") Incident incident, BindingResult result,
			ModelMap modelMap) {
		double priorityId = (double) (incident.getUrgencyId() + incident.getImpactId()) / 2;
		incident.setPriorityId((int) Math.ceil(priorityId));
		if (incident.getId() != 0) {
			incidentDAO.updateIncident(incident);
		} else {
			incidentDAO.createIncident(incident);
		}
		return "redirect:searchincident";
	}

	@RequestMapping(value = "/user/home", method = RequestMethod.GET)
	public ModelAndView searchincidentUser(Principal principal) {
		List<Incident> listIncident = new ArrayList<Incident>();

		listIncident = incidentDAO.getIncident("0", "0", "0", "0", "0", "0", principal.getName());

		ModelAndView mav = setModelAndViewForSearch(listIncident);

		return mav;
	}

	@RequestMapping(value = "/admin/{search}", method = RequestMethod.GET)
	public ModelAndView searchincident(@PathVariable(value = "search") String search, Principal principal) {
		List<Incident> listIncident = new ArrayList<Incident>();
		switch (search) {
		case "searchincident":
			listIncident = incidentDAO.getIncident("0", "0", "0", "0", "0", "0", null);
			break;
		case "myincident":
			listIncident = incidentDAO.getIncident("0", "0", "0", "0", "0", "0", principal.getName());
			break;
		case "escalatedincident":
			listIncident = incidentDAO.getIncident("0", "0", "0", "0", "0", "5", null);
			break;
		}
		ModelAndView mav = setModelAndViewForSearch(listIncident);
		mav.addObject("url", search);
		return mav;
	}

	private ModelAndView setModelAndViewForSearch(List<Incident> listIncident) {
		ModelAndView mav = new ModelAndView("admin/searchincident");
		List<Requestor> listRequestor = incidentDAO.getAllRequestor();
		List<Organization> listOrganization = incidentDAO.getAllOrganization();
		List<Priority> listPriority = incidentDAO.getAllPriority();
		List<Origin> listOrigin = incidentDAO.getAllOrigin();
		List<Type> listType = incidentDAO.getAllType();
		List<Status> listStatus = incidentDAO.getAllStatus();
		mav.addObject("listType", listType);
		mav.addObject("listOrigin", listOrigin);
		mav.addObject("listPriority", listPriority);
		mav.addObject("listOrganization", listOrganization);
		mav.addObject("listRequestor", listRequestor);
		mav.addObject("listStatus", listStatus);
		// paging
		int incidentPerPage = 10;
		int numberIncidents = listIncident.size();
		int numberPages = numberIncidents / incidentPerPage + (numberIncidents % incidentPerPage == 0 ? 0 : 1);
		int end;
		if (incidentPerPage > numberIncidents) {
			end = numberIncidents;
		} else {
			end = incidentPerPage;
		}

		List<Incident> incidentPage = incidentDAO.getIncidentByPage((ArrayList<Incident>) listIncident, 0, end);
		mav.addObject("listIncident", incidentPage);
		mav.addObject("numberPages", numberPages);
		mav.addObject("page", 1);
		return mav;
	}

	@RequestMapping(value = "/admin/search/{search}", method = RequestMethod.GET)
	public @ResponseBody String searchincident1(@RequestParam(value = "organId", required = false) String organId,

			@RequestParam(value = "requestorId", required = false) String requestorId,

			@RequestParam(value = "priorityId", required = false) String priorityId,

			@RequestParam(value = "originId", required = false) String originId,

			@RequestParam(value = "typeId", required = false) String typeId,

			@RequestParam(value = "statusId", required = false) String statusId,

			@RequestParam(value = "page", required = false) int page, @PathVariable(value = "search") String search,
			Principal principal, ModelMap modelMap) {
		if (search.equalsIgnoreCase("escalatedincident")) {
			statusId = "5";
		}
		String assignee = null;
		if (search.equalsIgnoreCase("myincident")) {
			assignee = principal.getName();
		}
		List<Incident> listIncident = incidentDAO.getIncident(organId, requestorId, priorityId, originId, typeId,
				statusId, assignee);
		int incidentPerPage = 10;
		int numberIncidents = listIncident.size();
		int numberPages = numberIncidents / incidentPerPage + (numberIncidents % incidentPerPage == 0 ? 0 : 1);
		int start = (page - 1) * incidentPerPage;
		int end;
		if (page * incidentPerPage > numberIncidents) {
			end = numberIncidents;
		} else {
			end = page * incidentPerPage;
		}

		List<Incident> incidentPage = incidentDAO.getIncidentByPage((ArrayList<Incident>) listIncident, start, end);
		String data = "";
		data += "<table class=\"table table-hover\">\r\n" + "						<thead>\r\n"
				+ "							<tr>\r\n" + "								<th scope=\"col\">Id</th>\r\n"
				+ "								<th scope=\"col\">Title</th>\r\n"
				+ "								<th scope=\"col\">Requestor</th>\r\n"
				+ "								<th scope=\"col\">Organization</th>\r\n"
				+ "								<th scope=\"col\">Type</th>\r\n"
				+ "								<th scope=\"col\">Status</th>\r\n"
				+ "								<th scope=\"col\">Urgency</th>\r\n"
				+ "								<th scope=\"col\">Priority</th>\r\n"
				+ "								<th scope=\"col\">DateStarted</th>\r\n"
				+ "								<th scope=\"col\">LastUpdate</th>\r\n"
				+ "								<th scope=\"col\">Action</th>\r\n"
				+ "							</tr>\r\n" + "						</thead>\r\n"
				+ "						<tbody>\r\n";
		for (Incident incident : incidentPage) {
			data += "<tr>\r\n" + "								<th scope=\"row\">" + incident.getId() + "</th>\r\n"
					+ "								<td>" + incident.getTitle() + "</td>\r\n"
					+ "								<td>" + incident.getRequestorName() + "</td>\r\n"
					+ "								<td>" + incident.getOrganizationName() + "</td>\r\n"
					+ "								<td>" + incident.getType() + "</td>\r\n"
					+ "								<td>" + incident.getStatus() + "</td>\r\n"
					+ "								<td style=\"color: "
					+ (incident.getUrgency().equals("Critical") ? "red"
							: incident.getUrgency().equals("High") ? "#CDCD00" : "#66CD00")
					+ "\">" + incident.getUrgency() + "</td>\r\n" + "								<td>"
					+ incident.getPriority() + "</td>\r\n" + "								<td>"
					+ incident.getDateStart() + "</td>\r\n" + "								<td>"
					+ (incident.getLastUpdate() == null ? "" : incident.getLastUpdate()) + "</td>\r\n"
					+ "								<td><a href=\"newincident?id=" + incident.getId()
					+ "\"><i class=\"fas fa-edit\"></i></a></td>\r\n" + "							</tr>\r\n";
		}
		data += "</tbody>\r\n" + "					</table>\r\n" + "					<nav>\r\n"
				+ "						<ul class=\"pagination pagination-primary justify-content-end\"\r\n"
				+ "							id=\"paging\">\r\n";
		if (page > 1) {
			data += "<li class=\"page-item\"><input type=\"radio\" name=\"page\"\r\n"
					+ "									id=\"prev\" value=\"${page - 1}\" hidden onclick=\"changePage("
					+ (page - 1) + ")\">\r\n"
					+ "									<label for=\"prev\"><a class=\"page-link\">Prev</a></label></li>\r\n";
		} else {
			data += "<li class=\"page-item\"><a class=\"page-link\" href=\"#\">Prev</a></li>\r\n";
		}
		for (int i = 1; i <= numberPages; i++) {
			data += "<li class=\"page-item " + (i == page ? "active" : "") + "\"><input\r\n"
					+ "								type=\"radio\" name=\"page\" id=\"" + i + "\" value=\"" + i
					+ "\" hidden onclick=\"changePage(" + i + ")\"\r\n" + "								> <label for=\""
					+ i + "\">\r\n" + "									<a class=\"page-link\">" + i + "</a>\r\n"
					+ "							</label></li>\r\n";
		}
		if (page < numberPages) {
			data += "<li class=\"page-item\"><input type=\"radio\" name=\"page\"\r\n"
					+ "									id=\"prev\" value=\"" + (page + 1)
					+ "\" hidden onclick=\"changePage(" + (page + 1) + ")\">\r\n"
					+ "									<label for=\"prev\"><a class=\"page-link\">Next</a></label></li>\r\n";
		} else {
			data += "<li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\r\n";
		}
		data += "</ul>\r\n" + "					</nav>";
		return data;
	}

}
