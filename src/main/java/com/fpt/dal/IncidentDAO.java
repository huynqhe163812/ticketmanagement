package com.fpt.dal;

import java.util.ArrayList;
import java.util.List;

import com.fpt.model.Impact;
import com.fpt.model.Incident;
import com.fpt.model.Organization;
import com.fpt.model.Origin;
import com.fpt.model.Priority;
import com.fpt.model.Requestor;
import com.fpt.model.Status;
import com.fpt.model.Type;
import com.fpt.model.Urgency;
import com.fpt.model.User;

public interface IncidentDAO {
	public List<User> getAllUser();

	public List<Incident> getIncident(String organId, String requestorId, String priorityId, String originId,
			String typeId, String statusId, String assignee);

	public List<Incident> getIncidentByPage(ArrayList<Incident> list, int start, int end);

	public int createIncident(Incident i);

	public int updateStatusIncident(int incidentId, int statusId);

	public int updateIncident(Incident i);

	public Incident getIncidentById(int id);

	public List<Status> getAllStatus();

	public List<Organization> getAllOrganization();

	public List<Requestor> getAllRequestor();

	public List<Origin> getAllOrigin();

	public List<Impact> getAllImpact();

	public List<Urgency> getAllUrgency();

	public List<Priority> getAllPriority();

	public List<Type> getAllType();
}
