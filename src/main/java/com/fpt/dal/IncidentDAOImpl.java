package com.fpt.dal;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.fpt.model.Impact;
import com.fpt.model.Incident;
import com.fpt.model.Organization;
import com.fpt.model.Origin;
import com.fpt.model.Priority;
import com.fpt.model.Requestor;
import com.fpt.model.Status;
import com.fpt.model.Type;
import com.fpt.model.Urgency;
import com.fpt.model.User;

public class IncidentDAOImpl implements IncidentDAO {

	private JdbcTemplate jdbcTemplate;

	public IncidentDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public static void main(String[] args) {
		/*
		 * DriverManagerDataSource dataSource = new DriverManagerDataSource();
		 * dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
		 * ; dataSource.
		 * setUrl("jdbc:sqlserver://localhost:1433;databaseName= TicketManagement");
		 * dataSource.setUsername("sa"); dataSource.setPassword("123");
		 * 
		 * JdbcTemplate jd = new JdbcTemplate(dataSource);
		 * 
		 * List<Status> li = jd.query("select [status] from tblStatus",
		 * BeanPropertyRowMapper.newInstance(Status.class));
		 * System.out.println(li.get(1).getId());
		 */
	}

	@Override
	public List<User> getAllUser() {
		return jdbcTemplate.query("select * from users", BeanPropertyRowMapper.newInstance(User.class));
	}

	@Override
	public List<Incident> getIncident(String organId, String requestorId, String priorityId, String originId,
			String typeId, String statusId, String assignee) {
		String sql = "select i.Id,i.OrganizationId,i.RequestorId, i.Reportor, i.StatusId,i.OriginId, i.Title, i.TypeId, i.Description, i.ImpactId, i.UrgencyId, i.PriorityId, i.DateStart, i.LastUpdate, i.Assignee, tblImpact.Impact,\r\n"
				+ "tblUrgency.Urgency, tblOrganization.OrganizationName, tblRequestor.RequestorName, tblPriority.Priority, tblStatus.status, tblOrigin.origin,\r\n"
				+ "tblType.type from tblIncident i join tblImpact on i.ImpactId =tblImpact.Id join tblUrgency on i.UrgencyId =tblUrgency.Id\r\n"
				+ "join tblOrganization on i.OrganizationId =tblOrganization.Id join tblRequestor on i.RequestorId =tblRequestor.Id \r\n"
				+ "join tblPriority on i.PriorityId =tblPriority.Id join tblStatus on i.StatusId =tblStatus.Id \r\n"
				+ "join tblOrigin on i.OriginId =tblOrigin.Id join tblType on i.TypeId =tblType.Id where 1=1";
		if (!organId.equalsIgnoreCase("0")) {
			sql += " and OrganizationId = " + organId;
		}
		if (!requestorId.equalsIgnoreCase("0")) {
			sql += " and RequestorId = " + requestorId;
		}
		if (!priorityId.equalsIgnoreCase("0")) {
			sql += " and PriorityId = " + priorityId;
		}
		if (!originId.equalsIgnoreCase("0")) {
			sql += " and OriginId = " + originId;
		}
		if (!typeId.equalsIgnoreCase("0")) {
			sql += " and TypeId = " + typeId;
		}

		if (!statusId.equalsIgnoreCase("0")) {
			sql += " and StatusId = " + statusId;
		}
		if (assignee != null) {
			sql += " and Assignee = '" + assignee + "'";
		}
		sql += " ORDER BY Id DESC";
		return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(Incident.class));
	}

	@Override
	public List<Incident> getIncidentByPage(ArrayList<Incident> list, int start, int end) {
		ArrayList<Incident> newList = new ArrayList<>();
		for (int i = start; i < end; i++) {
			newList.add(list.get(i));
		}
		return newList;
	}

	@Override
	public int createIncident(Incident i) {
		String sql = "insert into dbo.tblIncident values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		String assignee = null;
		int statusId = 1;
		if (!i.getAssignee().isEmpty()) {
			assignee = i.getAssignee();
			statusId = 4;
		}
		return jdbcTemplate.update(sql,
				new Object[] { i.getOrganizationId(), i.getRequestorId(), i.getReportor(), statusId, i.getOriginId(),
						i.getTitle(), i.getTypeId(), i.getDescription(), i.getImpactId(), i.getUrgencyId(),
						i.getPriorityId(), java.time.LocalDate.now().toString(), null, assignee });
	}

	@Override
	public int updateStatusIncident(int incidentId, int statusId) {
		String sql = "UPDATE [dbo].[tblIncident]\r\n" + "   SET [StatusId] = ?\r\n" + " WHERE Id=?";
		return jdbcTemplate.update(sql, new Object[] { statusId, incidentId });
	}

	@Override
	public int updateIncident(Incident i) {
		String sql = "UPDATE [dbo].[tblIncident]\r\n" + "   SET [OrganizationId] = ?\r\n"
				+ "      ,[RequestorId] = ?\r\n" + "      ,[Reportor] = ?\r\n" + "      ,[StatusId] = ?\r\n"
				+ "      ,[OriginId] = ?\r\n" + "      ,[Title] = ?\r\n" + "      ,[TypeId] = ?\r\n"
				+ "      ,[Description] = ?\r\n" + "      ,[ImpactId] = ?\r\n" + "      ,[UrgencyId] = ?\r\n"
				+ "      ,[PriorityId] = ?\r\n" + "      ,[LastUpdate] = ?\r\n" + "      ,[Assignee] = ?\r\n"
				+ " WHERE id =?";

		String assignee = null;
		int statusId = 1;
		if (!i.getAssignee().isEmpty()) {
			assignee = i.getAssignee();
			statusId = 4;
		}
		return jdbcTemplate.update(sql,
				new Object[] { i.getOrganizationId(), i.getRequestorId(), i.getReportor(), statusId, i.getOriginId(),
						i.getTitle(), i.getTypeId(), i.getDescription(), i.getImpactId(), i.getUrgencyId(),
						i.getPriorityId(), java.time.LocalDate.now().toString(), assignee, i.getId() });
	}

	@Override
	public Incident getIncidentById(int id) {
		try {
			Incident incident = jdbcTemplate.queryForObject(
					"SELECT i.[Id],[OrganizationId],[RequestorId],[Reportor],[StatusId],[OriginId],[Title],[TypeId],[Description],[ImpactId],[UrgencyId],[PriorityId],[DateStart],[LastUpdate],[Assignee],[status]\r\n"
							+ "	from tblIncident i join tblStatus s on i.StatusId=s.Id where i.Id=?",
					BeanPropertyRowMapper.newInstance(Incident.class), id);

			return incident;
		} catch (IncorrectResultSizeDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<Status> getAllStatus() {
		return jdbcTemplate.query("select * from tblStatus", BeanPropertyRowMapper.newInstance(Status.class));
	}

	@Override
	public List<Organization> getAllOrganization() {
		return jdbcTemplate.query("select * from tblOrganization",
				BeanPropertyRowMapper.newInstance(Organization.class));
	}

	@Override
	public List<Requestor> getAllRequestor() {
		return jdbcTemplate.query("select * from tblRequestor", BeanPropertyRowMapper.newInstance(Requestor.class));
	}

	@Override
	public List<Origin> getAllOrigin() {
		return jdbcTemplate.query("select * from tblOrigin", BeanPropertyRowMapper.newInstance(Origin.class));
	}

	@Override
	public List<Impact> getAllImpact() {
		return jdbcTemplate.query("select * from tblImpact", BeanPropertyRowMapper.newInstance(Impact.class));
	}

	@Override
	public List<Urgency> getAllUrgency() {
		return jdbcTemplate.query("select * from tblUrgency", BeanPropertyRowMapper.newInstance(Urgency.class));
	}

	@Override
	public List<Priority> getAllPriority() {
		return jdbcTemplate.query("select * from tblPriority", BeanPropertyRowMapper.newInstance(Priority.class));
	}

	@Override
	public List<Type> getAllType() {
		return jdbcTemplate.query("select * from tblType", BeanPropertyRowMapper.newInstance(Type.class));
	}

}
