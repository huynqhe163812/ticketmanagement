package com.fpt.model;

public class Impact {
	private int id;
	private String impact;
	
	public Impact() {
		super();
	}
	public Impact(int id, String impact) {
		super();
		this.id = id;
		this.impact = impact;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImpact() {
		return impact;
	}
	public void setImpact(String impact) {
		this.impact = impact;
	}
	
}
