package com.fpt.model;

import java.sql.Date;

public class Incident {
	private int id;
	private int organizationId;
	private String organizationName;
	private String reportor;
	private int requestorId;
	private String requestorName;
	private int statusId;
	private String status;
	private int originId;
	private String origin;
	private String title;
	private int typeId;
	private String type;
	private String description;
	private int impactId;
	private String impact;
	private int urgencyId;
	private String urgency;
	private int priorityId;
	private String priority;
	private Date dateStart;
	private Date lastUpdate;
	private String assignee;
	
	public Incident() {
		super();
	}

	public Incident(int id, String organizationName, String reportor, String requestorName, String status, String origin,
			String title, String type, String description, String impact, String urgency, String priority,
			Date dateStart, Date lastUpdate, String assignee) {
		super();
		this.id = id;
		this.organizationName = organizationName;
		this.reportor = reportor;
		this.requestorName = requestorName;
		this.status = status;
		this.origin = origin;
		this.title = title;
		this.type = type;
		this.description = description;
		this.impact = impact;
		this.urgency = urgency;
		this.priority = priority;
		this.dateStart = dateStart;
		this.lastUpdate = lastUpdate;
		this.assignee = assignee;
	}

	
	public Incident(int id, int organizationId, String reportor, int requestorId, String status, int originId,
			String title, int typeId, String description, int impactId, int urgencyId, int priorityId, Date dateStart,
			Date lastUpdate, String assignee) {
		super();
		this.id = id;
		this.organizationId = organizationId;
		this.reportor = reportor;
		this.requestorId = requestorId;
		this.status = status;
		this.originId = originId;
		this.title = title;
		this.typeId = typeId;
		this.description = description;
		this.impactId = impactId;
		this.urgencyId = urgencyId;
		this.priorityId = priorityId;
		this.dateStart = dateStart;
		this.lastUpdate = lastUpdate;
		this.assignee = assignee;
	}

	public Incident(int id, int organizationId, String organizationName, String reportor, int requestorId, String requestorName,
			int statusId, String status, int originId, String origin, String title, int typeId, String type,
			String description, int impactId, String impact, int urgencyId, String urgency, int priorityId,
			String priority, Date dateStart, Date lastUpdate, String assignee) {
		super();
		this.id = id;
		this.organizationId = organizationId;
		this.organizationName = organizationName;
		this.reportor = reportor;
		this.requestorId = requestorId;
		this.requestorName = requestorName;
		this.statusId = statusId;
		this.status = status;
		this.originId = originId;
		this.origin = origin;
		this.title = title;
		this.typeId = typeId;
		this.type = type;
		this.description = description;
		this.impactId = impactId;
		this.impact = impact;
		this.urgencyId = urgencyId;
		this.urgency = urgency;
		this.priorityId = priorityId;
		this.priority = priority;
		this.dateStart = dateStart;
		this.lastUpdate = lastUpdate;
		this.assignee = assignee;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}

	

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getReportor() {
		return reportor;
	}

	public void setReportor(String reportor) {
		this.reportor = reportor;
	}

	

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getOriginId() {
		return originId;
	}

	public void setOriginId(int originId) {
		this.originId = originId;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getImpactId() {
		return impactId;
	}

	public void setImpactId(int impactId) {
		this.impactId = impactId;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public int getUrgencyId() {
		return urgencyId;
	}

	public void setUrgencyId(int urgencyId) {
		this.urgencyId = urgencyId;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public int getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public int getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(int requestorId) {
		this.requestorId = requestorId;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	

}
