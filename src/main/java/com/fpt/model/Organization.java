package com.fpt.model;

public class Organization {
	private int id;
	private String organizationName;
	
	public Organization() {
		super();
	}
	public Organization(int id, String organizationName) {
		super();
		this.id = id;
		this.organizationName = organizationName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	
}
