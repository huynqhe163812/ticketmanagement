package com.fpt.model;

public class Origin {
	private int id;
	private String origin;
	
	public Origin() {
		super();
	}
	public Origin(int id, String origin) {
		super();
		this.id = id;
		this.origin = origin;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
}
