package com.fpt.model;

public class Requestor {
	private int id;
	private String requestorName;
	
	public Requestor() {
		super();
	}
	public Requestor(int id, String requestorName) {
		super();
		this.id = id;
		this.requestorName = requestorName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	
}
