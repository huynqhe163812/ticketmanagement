package com.fpt.model;

public class Urgency {
	private int id;
	private String urgency;
	
	public Urgency() {
		super();
	}
	public Urgency(int id, String urgency) {
		super();
		this.id = id;
		this.urgency = urgency;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUrgency() {
		return urgency;
	}
	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}
	
}
