<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<body>

	<div id="wrapper">
		<!-- Sidebar -->
		<ul class="sidebar navbar-nav toggled">
			<li class="nav-item"><a class="nav-link"
				href="<c:url value='/homepage' />"> <i
					class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
			</a></li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="pagesDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> <i class="fas fa-fw fa-folder"></i> <span>Shortcuts</span>
			</a>
				<div class="dropdown-menu" aria-labelledby="pagesDropdown">
					<h6 class="dropdown-header">Login Screens:</h6>
					<a class="dropdown-item" href="<c:url value='/admin/myincident'/>">Incidents
						assigned to me</a> <a class="dropdown-item" href="register.html">Register</a>
					<a class="dropdown-item" href="forgot-password.html">Forgot
						Password</a>
					<div class="dropdown-divider"></div>
					<h6 class="dropdown-header">Other Pages:</h6>
					<a class="dropdown-item" href="404.html">404 Page</a> <a
						class="dropdown-item" href="blank.html">Blank Page</a>
				</div></li>
			<li class="nav-item"><a class="nav-link"
				href="<c:url value='/admin/newincident' />"> <i
					class="fas fa-fw fa-chart-area"></i> <span>New Incident</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="<c:url value='/admin/searchincident' />"> <i
					class="fas fa-fw fa-table"></i> <span>Search Incident</span>
			</a></li>
			<li class="nav-item active"><a class="nav-link"
				href="<c:url value='/admin/escalatedincident'/>"> <i
					class="fas fa-hourglass-end"></i> <span>Escalated Incident</span>
			</a></li>
		</ul>

		<div id="content-wrapper">

			<div class="container-fluid">

				<!-- Breadcrumbs-->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Escalated Incident</li>
				</ol>

				<div id="table-paging">
					<table class="table table-hover">
						<thead>
							<tr>
								<th scope="col">Id</th>
								<th scope="col">Title</th>
								<th scope="col">Requestor</th>
								<th scope="col">Organization</th>
								<th scope="col">Type</th>
								<th scope="col">Status</th>
								<th scope="col">Urgency</th>
								<th scope="col">Assignee</th>
								<th scope="col">DateStarted</th>
								<th scope="col">LastUpdate</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listIncident}" var="incident">
								<tr>
									<th scope="row">${incident.getId()}</th>
									<td>${incident.getTitle()}</td>
									<td>${incident.getRequestorName()}</td>
									<td>${incident.getOrganizationName()}</td>
									<td>${incident.getType()}</td>
									<td>${incident.getStatus()}</td>
									<td style="color: ${incident.getUrgency()==" Critical" ? 'red':
										incident.getUrgency()=="High"?'#CDCD00':'#66CD00'}" >${incident.getUrgency()}</td>
									<td>${incident.getAssignee()}</td>
									<td>${incident.getDateStart()}</td>
									<td>${incident.getLastUpdate()}</td>
									<td><a href="<c:url value='/admin/newincident?id=${incident.getId()}'/>"><i
											class="fas fa-edit"></i></a></td>
								</tr>
							</c:forEach>


						</tbody>
					</table>
					<nav>
						<ul class="pagination pagination-primary justify-content-end"
							id="paging">
							<c:choose>
								<c:when test="${page > 1}">
									<li class="page-item"><input type="radio" name="page"
										id="prev" value="${page - 1}" hidden
										onclick="changePage(${page - 1})"> <label for="prev"><a
											class="page-link">Prev</a></label></li>
								</c:when>
								<c:otherwise>
									<li class="page-item"><a class="page-link" href="#">Prev</a></li>
								</c:otherwise>
							</c:choose>
							<c:forEach begin="${1}" end="${numberPages}" var="i">
								<li class="page-item ${i == page ? "active" : ""}"><input
									type="radio" name="page" id="${i}" value="${i}" hidden
									onclick="changePage(${i})"> <label for="${i}">
										<a class="page-link">${i}</a>
								</label></li>
							</c:forEach>
							<c:choose>
								<c:when test="${page < numberPages}">
									<li class="page-item"><input type="radio" name="page"
										id="prev" value="${page + 1}" hidden
										onclick="changePage(${page+1})"> <label for="prev"><a
											class="page-link">Next</a></label></li>
								</c:when>
								<c:otherwise>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</nav>
				</div>


			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<footer class="sticky-footer">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright © Your Website 2019</span>
					</div>
				</div>
			</footer>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top"> <i
		class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready
					to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button"
						data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="login.html">Logout</a>
				</div>
			</div>
		</div>
	</div>

	<script>
	 	
	</script>
</body>
