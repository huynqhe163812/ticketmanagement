<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<body>

	<div id="wrapper">
		<!-- Sidebar -->
		<ul class="sidebar navbar-nav toggled">
			<li class="nav-item active"><a class="nav-link"
				href="homepage"> <i class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="admin/searchincident">
					<i class="fas fa-fw fa-table"></i> <span>Search Incident</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="<c:url value='/admin/newincident'/>">
					<i class="fas fa-fw fa-chart-area"></i> <span>New Incident</span>
			</a></li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="pagesDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> <i class="fas fa-fw fa-folder"></i> <span>Shortcuts</span>
			</a>
				<div class="dropdown-menu" aria-labelledby="pagesDropdown">
					<h6 class="dropdown-header">Screens:</h6>
					<a class="dropdown-item" href="<c:url value='/admin/myincident'/>">Incidents
						assigned to me</a> 
						<a class="dropdown-item" href="<c:url value='/admin/escalatedincident'/>">Escalated Incident</a>
					<a class="dropdown-item" href="forgot-password.html">Forgot
						Password</a>
					<div class="dropdown-divider"></div>
					<h6 class="dropdown-header">Other Pages:</h6>
					<a class="dropdown-item" href="404.html">404 Page</a> <a
						class="dropdown-item" href="blank.html">Blank Page</a>
				</div></li>
				
			
			
		</ul>

		<div id="content-wrapper">

			<div class="container-fluid">

				<!-- Breadcrumbs-->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
					<li class="breadcrumb-item active">Overview</li>
				</ol>

				<!-- Icon Cards-->
				<div class="row card-body">
					<div class="col-md-6 card">
						<div class="card-header1">
							<h4>Open Incidents by Status</h4>
						</div>
						Total: ? objects
						<div style="border: 4px solid rgba(0, 0, 0, 0.075)">
							<table class="table table-hover table-striped">
								<tr class="table-active">
									<th>Status</th>
									<th>Count</th>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-6 card">
						<div class="card-header1">
							<h4>Open Incidents by Status</h4>
						</div>
						Total: ? objects
						<div style="border: 4px solid rgba(0, 0, 0, 0.075)">
							<table class="table table-hover table-striped">
								<tr class="table-active">
									<th>Status</th>
									<th>Count</th>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-6 card">
						<div class="card-header1">
							<h4>Open Incidents by Status</h4>
						</div>
						Total: ? objects
						<div style="border: 4px solid rgba(0, 0, 0, 0.075)">
							<table class="table table-hover table-striped">
								<tr class="table-active">
									<th>Status</th>
									<th>Count</th>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
								<tr>
									<td>123</td>
									<td>123</td>
								</tr>
							</table>
						</div>
					</div>
				</div>

				<!-- Area Chart Example-->
				<div class="card mb-3">
					<div class="card-header">
						<i class="fas fa-chart-area"></i> Area Chart Example
					</div>
					<div class="card-body">
						<canvas id="myAreaChart" width="100%" height="30"></canvas>
					</div>
					<div class="card-footer small text-muted">Updated yesterday
						at 11:59 PM</div>
				</div>

				
			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<footer class="sticky-footer">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright � Your Website 2019</span>
					</div>
				</div>
			</footer>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top"> <i
		class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">�</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready
					to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button"
						data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="login.html">Logout</a>
				</div>
			</div>
		</div>
	</div>

</body>
