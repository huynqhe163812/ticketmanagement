<%@ include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<body>

	<div id="wrapper">
		<!-- Sidebar -->
		<ul class="sidebar navbar-nav toggled">
			<li class="nav-item"><a class="nav-link"
				href="<c:url value='/homepage' />"> <i
					class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="<c:url value='/admin/searchincident' />"> <i
					class="fas fa-fw fa-table"></i> <span>Search Incident</span>
			</a></li>
			<li class="nav-item  active"><a class="nav-link"
				href="<c:url value='/admin/newincident' />"> <i
					class="fas fa-fw fa-chart-area"></i> <span>New Incident</span>
			</a></li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="pagesDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> <i class="fas fa-fw fa-folder"></i> <span>Shortcuts</span>
			</a>
				<div class="dropdown-menu" aria-labelledby="pagesDropdown">
					<h6 class="dropdown-header">Screens:</h6>
					<a class="dropdown-item" href="<c:url value='/admin/myincident'/>">Incidents
						assigned to me</a> <a class="dropdown-item"
						href="<c:url value='/admin/escalatedincident'/>">Escalated
						Incident</a> <a class="dropdown-item" href="forgot-password.html">Forgot
						Password</a>
					<div class="dropdown-divider"></div>
					<h6 class="dropdown-header">Other Pages:</h6>
					<a class="dropdown-item" href="404.html">404 Page</a> <a
						class="dropdown-item" href="blank.html">Blank Page</a>
				</div></li>
		</ul>

		<div id="content-wrapper">

			<div class="container-fluid">

				<!-- Breadcrumbs-->
				<ol class="breadcrumb">
					<li class="breadcrumb-item">Create a new incident</li>
				</ol>

				<!-- Icon Cards-->
				<form:form action="newincident" method="POST"
					modelAttribute="incident">
					<div class="row card-body">
						<div class="col-md-12">
							<c:if test="${enable}">
								<button type="button" style="padding: 0 1%" data-toggle="modal"
									data-target="#modal-assignee">Assign</button>
							</c:if>

						</div>
						<div class="col-md-6">
							<fieldset>
								<legend>General Information</legend>
								<table class="new_incident_table">
									<tr>
										<td><label for="organization">Organization</label></td>
										<td><form:select path="organizationId" id="organization">
												<form:option value="0" label="--Select one--"></form:option>
												<form:options items="${listOrganization}" itemValue="id"
													itemLabel="organizationName" />
											</form:select>
											<div style="display: inline-flex;">
												<button type="button" style="padding: 0 98%"
													class="btn btn-primary">
													<i class="fas fa-plus"></i>
												</button>
											</div></td>
									</tr>
									<tr>
										<td><label for="requestor">Requestor</label></td>
										<td><form:select path="requestorId" id="requestor">
												<form:option value="0" label="--Select one--"></form:option>
												<form:options items="${listRequestor}" itemValue="id"
													itemLabel="requestorName" />
											</form:select>
											<div style="display: inline-flex;">
												<button type="button" style="padding: 0 98%"
													class="btn btn-primary">
													<i class="fas fa-plus"></i>
												</button>
											</div></td>
									</tr>
									<tr>
										<td><label for="field-1">Reported by</label></td>
										<td><form:input path="reportor" id="field-1" /></td>
									</tr>
									<tr>
										<td><label for="field-2">Status</label></td>
										<td>${incident.getStatus()}</td>
									</tr>
									<tr>
										<td><label for="origin">Origin</label></td>
										<td><form:select path="originId" id="origin">
												<form:option value="0" label="--Select one--"></form:option>
												<form:options items="${listOrigin}" itemValue="id"
													itemLabel="origin" />
											</form:select></td>
									</tr>
									<tr>
										<td><label for="field-1">Title</label></td>
										<td><form:input path="title" id="field-1" /></td>
									</tr>
									<tr>
										<td><label for="type">Type</label></td>
										<td><form:select path="typeId" id="type">
												<form:option value="0" label="--Select one--"></form:option>
												<form:options items="${listType}" itemValue="id"
													itemLabel="type" />
											</form:select></td>
									</tr>

								</table>
								<label>Description</label> <br />
								<form:textarea path="description" style="width: 100%" rows="5" />
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset>
								<legend>Qualification</legend>
								<table class="new_incident_table">
									<tr>
										<td><label for="impact">Impact</label></td>
										<td><form:select path="impactId" id="impact">
												<form:option value="0" label="--Select one--"></form:option>
												<c:forEach items="${listImpact}" var="impact">
													<form:option value="${impact.getId()}"
														label="${impact.getId()}-${impact.getImpact()}"></form:option>
												</c:forEach>

											</form:select></td>
									</tr>
									<tr>
										<td><label for="urgency">Urgency</label></td>
										<td><form:select path="urgencyId" id="urgency">
												<form:option value="0" label="--Select one--"></form:option>
												<c:forEach items="${listUrgency}" var="urgency">
													<form:option value="${urgency.getId()}"
														label="${urgency.getId()}-${urgency.getUrgency()}"></form:option>
												</c:forEach>

											</form:select></td>
									</tr>
									<tr>
										<td><label for="field-2">Priority</label></td>
										<td>3-Medium</td>
									</tr>
								</table>
							</fieldset>
							<fieldset>
								<legend>Dates</legend>
								<table class="new_incident_table">
									<tr>
										<td style="width: 25%">Start date</td>
										<td>${incident.getDateStart()}</td>
									</tr>
									<tr>
										<td>Last update</td>
										<td>${incident.getLastUpdate()}</td>
									</tr>
								</table>

							</fieldset>

							<fieldset>
								<legend>Contact</legend>
								<table class="new_incident_table">
									<tr>
										<td style="width: 25%">Assignee</td>
										<td><a href="" id="assignee_name">${incident.getAssignee()}</a>


											<div class="modal fade" id="modal-assignee" tabindex="-1"
												role="dialog" aria-labelledby="exampleModalCenterTitle"
												aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered"
													role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title" id="exampleModalLongTitle">Update
																Assignee</h5>
															<button type="button" class="close" data-dismiss="modal"
																aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															Assignee:
															<form:input list="brow" path="assignee"
																onchange="updateAssignee()" />
															<datalist id="brow">
																<c:forEach items="${listUser}" var="user">
																	<option
																		onclick="updateAssignee('${user.getUsername()}')"
																		value="${user.getUsername()}">
																</c:forEach>

															</datalist>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-secondary"
																data-dismiss="modal">Ok</button>
														</div>
													</div>
												</div>
											</div></td>

									</tr>
									<!-- <tr>
										<td><label for="field-2">ten</label></td>
										<td><input list="brow"> <datalist id="brow">
												<option value="Internet Explorer">
												<option value="Firefox">
												<option value="Chrome">
												<option value="Opera">
												<option value="Safari">
											</datalist></td>
									</tr> -->
								</table>

							</fieldset>

						</div>
						<form:hidden path="id" />
						<c:if test="${incident.id!=0}">
							<div class="col-md-12 d-flex justify-content-center">
								<input class="btn btn-primary" type="submit" value="Update"
									style="margin-right: 20px">
								<c:if
									test="${pageContext.request.userPrincipal.name == incident.getAssignee() }">
									<a
										href="<c:url value='/admin/solveincident?incidentId=${incident.id}&statusId=3'/>"
										style="color: white"><button type="button"
											class="btn btn-primary" style="margin-right: 20px">Complete</button></a>
									<a
										href="<c:url value='/admin/solveincident?incidentId=${incident.id}&statusId=5'/>"
										style="color: white"><button type="button"
											class="btn btn-primary">Escalate</button></a>
								</c:if>
							</div>
						</c:if>

						<c:if test="${incident.id == 0}">
							<div class="col-md-12 d-flex justify-content-center">
								<button class="btn btn-primary">Add</button>
							</div>
						</c:if>

					</div>
				</form:form>



				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<footer class="sticky-footer">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<span>Copyright � Your Website 2019</span>
						</div>
					</div>
				</footer>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top"> <i
			class="fas fa-angle-up"></i>
		</a>

		<!-- Logout Modal-->
		<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ready to
							Leave?</h5>
						<button class="close" type="button" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">�</span>
						</button>
					</div>
					<div class="modal-body">Select "Logout" below if you are
						ready to end your current session.</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button"
							data-dismiss="modal">Cancel</button>
						<a class="btn btn-primary" href="login.html">Logout</a>
					</div>


				</div>
			</div>
		</div>
	</div>
	<script>
		function complete() {

		}

		function updateAssignee() {
			document.getElementById("assignee_name").innerHTML = document
					.getElementById("assignee").value
					+ "";
		}
	</script>
</body>
