<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<body>

	<div id="wrapper">
		<!-- Sidebar -->
		<ul class="sidebar navbar-nav toggled">
			<li class="nav-item"><a class="nav-link"
				href="<c:url value='/homepage' />"> <i
					class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
			</a></li>
			<li class="nav-item dropdown ${url=='searchincident'? '':'active'}"><a
				class="nav-link dropdown-toggle" href="#" id="pagesDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> <i class="fas fa-fw fa-folder"></i> <span>Shortcuts</span>
			</a>
				<div class="dropdown-menu" aria-labelledby="pagesDropdown">
					<h6 class="dropdown-header">Screens:</h6>
					<a class="dropdown-item ${url=='myincident'? 'active':''}" href="<c:url value='/admin/myincident'/>">Incidents assigned to me</a> 
						<a class="dropdown-item ${url=='escalatedincident'? 'active':''}" href="<c:url value='/admin/escalatedincident'/>">Escalated Incident</a>
					<a class="dropdown-item" href="forgot-password.html">Forgot
						Password</a>
					<div class="dropdown-divider"></div>
					<h6 class="dropdown-header">Other Pages:</h6>
					<a class="dropdown-item" href="404.html">404 Page</a> <a
						class="dropdown-item" href="blank.html">Blank Page</a>
				</div></li>
			<li class="nav-item"><a class="nav-link"
				href="<c:url value='/admin/newincident' />"> <i
					class="fas fa-fw fa-chart-area"></i> <span>New Incident</span>
			</a></li>
			<li class="nav-item ${url=='searchincident'? 'active':'' }"><a class="nav-link"
				href="<c:url value='/admin/searchincident' />"> <i
					class="fas fa-fw fa-table"></i> <span>Search Incident</span>
			</a></li>
		</ul>

		<div id="content-wrapper">

			<div class="container-fluid">

				<!-- Breadcrumbs-->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<c:url value='/homepage' />">Dashboard</a></li>
					<li class="breadcrumb-item active">${url=='escalatedincident'? 'Escalated Incident': url=='myincident'?'Incidents assigned to me':'Incident List'}</li>
				</ol>
				<div class="criteria-content">
					<button type="button" class="btn btn-secondary" data-toggle="modal"
						data-target="#modal-criteria">
						Add new criteria <i class="fas fa-plus"></i>
					</button>

					<div class="modal fade" id="modal-criteria" tabindex="-1"
						role="dialog" aria-labelledby="exampleModalCenterTitle"
						aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLongTitle">Add
										criteria</h5>
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<button class="btn btn-secondary" id="organ"
										onclick="addCriteria('organ')">Organization</button>
									<button class="btn btn-secondary" id="requestor"
										onclick="addCriteria('requestor')">Requestor</button>
									<button class="btn btn-secondary" id="priority"
										onclick="addCriteria('priority')">Priority</button>
									<button class="btn btn-secondary" id="origin"
										onclick="addCriteria('origin')">Origin</button>
									<button class="btn btn-secondary" id="type"
										onclick="addCriteria('type')">Type</button>
										<button class="btn btn-secondary" id="status"
										onclick="addCriteria('status')">Status</button>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary"
										data-dismiss="modal">Ok</button>
								</div>
							</div>
						</div>
					</div>
					
					<div class="dropdown criteria" id="organ1">
						<button class="btn btn-secondary dropdown-toggle" type="button"
							id="dropdownMenuButton" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">
							Organization: <span id="organ-criteria">Any</span>
							<div class="x-close" style="display: inline-block">
								<a onclick="deleteCriteria(0,'organ')">&times;</a>
							</div>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<c:forEach items="${listOrganization}" var="organization">
								<a class="dropdown-item"
									onclick="filterData(0,'${organization.getId()}','${organization.getOrganizationName()}','organ-criteria',1)">${organization.getOrganizationName()}</a>
							</c:forEach>
						</div>
					</div>

					<div class="dropdown criteria" id="requestor1">
						<button class="btn btn-secondary dropdown-toggle" type="button"
							id="dropdownMenuButton" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">
							Requestor: <span id="requestor-criteria">Any</span>
							<div class="x-close" style="display: inline-block">
								<a onclick="deleteCriteria(1,'requestor')">&times;</a>
							</div>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<c:forEach items="${listRequestor}" var="requestor">
								<a class="dropdown-item"
									onclick="filterData(1,'${requestor.getId()}','${requestor.getRequestorName()}','requestor-criteria',1)">${requestor.getRequestorName()}</a>
							</c:forEach>
						</div>
					</div>

					<div class="dropdown criteria" id="priority1">
						<button class="btn btn-secondary dropdown-toggle" type="button"
							id="dropdownMenuButton" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">
							Priority: <span id="priority-criteria">Any</span>
							<div class="x-close" style="display: inline-block">
								<a onclick="deleteCriteria(2,'priority')">&times;</a>
							</div>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<c:forEach items="${listPriority}" var="priority">
								<a class="dropdown-item"
									onclick="filterData(2,'${priority.getId()}','${priority.getPriority()}','priority-criteria',1)">${priority.getPriority()}</a>
							</c:forEach>
						</div>
					</div>

					<div class="dropdown criteria" id="origin1">
						<button class="btn btn-secondary dropdown-toggle" type="button"
							id="dropdownMenuButton" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">
							Origin: <span id="origin-criteria">Any</span>
							<div class="x-close" style="display: inline-block">
								<a onclick="deleteCriteria(3,'origin')">&times;</a>
							</div>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<c:forEach items="${listOrigin}" var="origin">
								<a class="dropdown-item"
									onclick="filterData(3,'${origin.getId()}','${origin.getOrigin()}','origin-criteria',1)">${origin.getOrigin()}</a>
							</c:forEach>
						</div>
					</div>

					<div class="dropdown criteria type1" id="type1">
						<button class="btn btn-secondary dropdown-toggle" type="button"
							id="dropdownMenuButton" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">
							Type: <span id="type-criteria">Any</span>
							<div class="x-close" style="display: inline-block">
								<a onclick="deleteCriteria(4,'type')">&times;</a>
							</div>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<c:forEach items="${listType}" var="type">
								<a class="dropdown-item"
									onclick="filterData(4,'${type.getId()}','${type.getType()}','type-criteria',1)">${type.getType()}</a>
							</c:forEach>
						</div>
					</div>
					<div class="dropdown criteria" id="status1">
						<button class="btn btn-secondary dropdown-toggle" type="button"
							id="dropdownMenuButton" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">
							Status: <span id="status-criteria">Any</span>
							<div class="x-close" style="display: inline-block">
								<a onclick="deleteCriteria(5,'status')">&times;</a>
							</div>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<c:forEach items="${listStatus}" var="status">
								<a class="dropdown-item"
									onclick="filterData(5,'${status.getId()}','${status.getStatus()}','status-criteria',1)">${status.getStatus()}</a>
							</c:forEach>
						</div>
					</div>
					<button class="btn btn-secondary" onclick="refresh()">
						<i class="fas fa-sync-alt"></i>
					</button>

				</div>
				<div id="table-paging">
					<table class="table table-hover">
						<thead>
							<tr>
								<th scope="col">Id</th>
								<th scope="col">Title</th>
								<th scope="col">Requestor</th>
								<th scope="col">Organization</th>
								<th scope="col">Type</th>
								<th scope="col">Status</th>
								<th scope="col">Urgency</th>
								<th scope="col">Priority</th>
								<th scope="col">DateStarted</th>
								<th scope="col">LastUpdate</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listIncident}" var="incident">
								<tr>
									<th scope="row">${incident.getId()}</th>
									<td>${incident.getTitle()}</td>
									<td>${incident.getRequestorName()}</td>
									<td>${incident.getOrganizationName()}</td>
									<td>${incident.getType()}</td>
									<td>${incident.getStatus()}</td>
									<td style="color: ${incident.getUrgency()=="Critical" ? 'red':
										incident.getUrgency()=="High"?'#CDCD00':'#66CD00'}" >${incident.getUrgency()}</td>
									<td>${incident.getPriority()}</td>
									<td>${incident.getDateStart()}</td>
									<td>${incident.getLastUpdate()}</td>
									<td><a href="<c:url value='/admin/newincident?id=${incident.getId()}'/>"><i
											class="fas fa-edit"></i></a>
								</tr>
							</c:forEach>


						</tbody>
					</table>
					<nav>
						<ul class="pagination pagination-primary justify-content-end"
							id="paging">
							<c:choose>
								<c:when test="${page > 1}">
									<li class="page-item"><input type="radio" name="page"
										id="prev" value="${page - 1}" hidden
										onclick="changePage(${page - 1})"> <label for="prev"><a
											class="page-link">Prev</a></label></li>
								</c:when>
								<c:otherwise>
									<li class="page-item"><a class="page-link" href="#">Prev</a></li>
								</c:otherwise>
							</c:choose>
							<c:forEach begin="${1}" end="${numberPages}" var="i">
								<li class="page-item ${i == page ? "active" : ""}"><input
									type="radio" name="page" id="${i}" value="${i}" hidden
									onclick="changePage(${i})"> <label for="${i}">
										<a class="page-link">${i}</a>
								</label></li>
							</c:forEach>
							<c:choose>
								<c:when test="${page < numberPages}">
									<li class="page-item"><input type="radio" name="page"
										id="prev" value="${page + 1}" hidden
										onclick="changePage(${page+1})"> <label for="prev"><a
											class="page-link">Next</a></label></li>
								</c:when>
								<c:otherwise>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</nav>
				</div>

			
			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<footer class="sticky-footer">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright © Your Website 2019</span>
					</div>
				</div>
			</footer>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top"> <i
		class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready
					to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button"
						data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="login.html">Logout</a>
				</div>
			</div>
		</div>
	</div>
	
	<script>
	 	var filter = ["0", "0","0","0","0","0"];
	 	console.log(filter);
	 	
	 	
	 	function add(){
	 		$.ajax({
                url: "searchincident",
                type: 'POST',
                data: {
                    organId: filter[0],
                    requestorId: filter[1],
                    priorityId: filter[2],
                    originId: filter[3],
                    typeId: filter[4],
                    statusId: filter[5],
                },
                success: function (dat) {
                	console.log(dat);
                	document.getElementById("body-table1").innerHTML = dat;
                }
            });
	 	}
	 	
	 	function changePage(page){
	 		$.ajax({
                url: "search/${url}",
                type: 'GET',
                data: {
                	organId: filter[0],
                    requestorId: filter[1],
                    priorityId: filter[2],
                    originId: filter[3],
                    typeId: filter[4],
                    statusId: filter[5],
                    page: page
                },
                success: function (dat) {
                	console.log("nnnn");
                	document.getElementById("table-paging").innerHTML = dat;
                }
            }).fail(function (Response) {
            	console.log("jjjjjj");
            });
	 	}
	 	
	 	function filterData(index,data, criteria, name_criteria, page){
	 		filter[index]=data;
	 		document.getElementById(name_criteria).innerHTML = criteria;
	 		$.ajax({
                url: "search/${url}",
                type: 'GET',
                data: {
                	organId: filter[0],
                    requestorId: filter[1],
                    priorityId: filter[2],
                    originId: filter[3],
                    typeId: filter[4],
                    statusId: filter[5],
                    page: page
                },
                success: function (dat) {
                	document.getElementById("table-paging").innerHTML = dat;
                }
            });
	 	}
	 	
	 	function refresh(){
	 		$.ajax({
                url: "nn",
                type: 'POST',
                success: function (dat) {
                	console.log(dat);
                }
            });
	 	}
	 	
		function addCriteria(criteria) {
			document.getElementById(criteria).style.display = "none";
			document.getElementById(criteria + "1").style.display = "inline-block";
		}

		function fun(hh) {
			document.getElementById("organ").innerHTML = hh
		}
		function deleteCriteria(index,criteria) {
			filterData(index,"0","Any",criteria+"-criteria",1);
			document.getElementById(criteria).style.display = "inline-block";
			document.getElementById(criteria + "1").style.display = "none";
		}
	</script>
</body>
