<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Trang chủ</title>

<!-- Custom fonts for this template-->
<link
	href="<c:url value='/template/admin/vendor/fontawesome-free/css/all.min.css'/>"
	rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link
	href="<c:url value='/template/admin/vendor/datatables/dataTables.bootstrap4.css'/>"
	rel="stylesheet" type="text/css">

<!-- Custom styles for this template-->
<link href="<c:url value='/template/admin/css/sb-admin.css'/>"
	rel="stylesheet" type="text/css">

<style>
.row .card-header1 {
	background-color: rgba(0, 0, 0, 0.075);
	margin-top: 15px;
	margin-bottom: 15px;
	text-align: center;
}

.card {
	margin-top: 20px;
}

.table th, .table td {
	border-right: 1px solid white;
}

fieldset {
	margin: 8px;
	border: 1px solid silver;
	padding: 8px;
	border-radius: 4px;
}

legend {
	padding: 2px;
	width: auto;
}

.new_incident_table {
	width: 100%;
}

.new_incident_table tr td select {
	width: 80%;
}

.dropdown-toggle::after{
	display:none;
}
.x-close::before{
	display: inline-block;
    margin-left: 0.255em;
    vertical-align: 0.255em;
    content: "";
    border-top: 0.3em solid;
    border-right: 0.3em solid transparent;
    border-bottom: 0;
    border-left: 0.3em solid transparent;
}
.criteria{
	display:none;
}

//paging

.pagination.pagination-primary .page-item.active .page-link {
    background-color: #435ebe;
    border-color: #435ebe;
    box-shadow: 0 2px 5px rgba(67, 94, 190, .3)
}

.pagination.pagination-secondary .page-item.active .page-link {
    background-color: #6c757d;
    border-color: #6c757d;
    box-shadow: 0 2px 5px rgba(108, 117, 125, .3)
}

.pagination.pagination-success .page-item.active .page-link {
    background-color: #198754;
    border-color: #198754;
    box-shadow: 0 2px 5px rgba(25, 135, 84, .3)
}

.pagination.pagination-info .page-item.active .page-link {
    background-color: #0dcaf0;
    border-color: #0dcaf0;
    box-shadow: 0 2px 5px rgba(13, 202, 240, .3)
}

.pagination.pagination-warning .page-item.active .page-link {
    background-color: #ffc107;
    border-color: #ffc107;
    box-shadow: 0 2px 5px rgba(255, 193, 7, .3)
}

.pagination.pagination-danger .page-item.active .page-link {
    background-color: #dc3545;
    border-color: #dc3545;
    box-shadow: 0 2px 5px rgba(220, 53, 69, .3)
}

.pagination.pagination-light .page-item.active .page-link {
    background-color: #f8f9fa;
    border-color: #f8f9fa;
    box-shadow: 0 2px 5px rgba(248, 249, 250, .3)
}

.pagination.pagination-dark .page-item.active .page-link {
    background-color: #212529;
    border-color: #212529;
    box-shadow: 0 2px 5px rgba(33, 37, 41, .3)
}

.page-item:not(.active) .page-link:hover {
    color: #000
}

.page-item i,
.page-item svg {
    font-size: 13px;
    width: 13px;
    height: 13px
}

.page-item .page-link {
    font-size: .875rem
}

.page-item .page-link:focus {
    box-shadow: none
}

.page-item:first-child {
    margin-right: .4rem
}

.page-item:last-child {
    margin-left: .4rem
}
</style>
</head>
<body id="page-top">

	<%@ include file="/common/admin/header.jsp"%>

	<dec:body />

	<!-- Bootstrap core JavaScript-->
	<script
		src="<c:url value='/template/admin/vendor/jquery/jquery.min.js'/>"></script>
	<script
		src="<c:url value='/template/admin/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>

	<!-- Core plugin JavaScript-->
	<script
		src="<c:url value='/template/admin/vendor/jquery-easing/jquery.easing.min.js'/>"></script>

	<!-- Page level plugin JavaScript-->
	<script
		src="<c:url value='/template/admin/vendor/chart.js/Chart.min.js'/>"></script>
	<script
		src="<c:url value='/template/admin/vendor/datatables/jquery.dataTables.js'/>"></script>
	<script
		src="<c:url value='/template/admin/vendor/datatables/dataTables.bootstrap4.js'/>"></script>

	<!-- Custom scripts for all pages-->
	<script src="<c:url value='/template/admin/js/sb-admin.min.js'/>"></script>

	<!-- Demo scripts for this page-->
	<script
		src="<c:url value='/template/admin/js/demo/datatables-demo.js'/>"></script>
	<script
		src="<c:url value='/template/admin/js/demo/chart-area-demo.js'/>"></script>
</body>
</html>