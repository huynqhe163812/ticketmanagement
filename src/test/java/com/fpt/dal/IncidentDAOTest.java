package com.fpt.dal;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.fpt.model.Impact;
import com.fpt.model.Incident;
import com.fpt.model.Organization;
import com.fpt.model.Origin;
import com.fpt.model.Priority;
import com.fpt.model.Requestor;
import com.fpt.model.Status;
import com.fpt.model.Type;
import com.fpt.model.Urgency;
import com.fpt.model.User;

class IncidentDAOTest {

	private DriverManagerDataSource dataSource;
	private IncidentDAO dao;

	@BeforeEach
	void setipBeforeEach() {
		dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		dataSource.setUrl("jdbc:sqlserver://localhost:1433;databaseName= TicketManagement");
		dataSource.setUsername("sa");
		dataSource.setPassword("123");

		dao = new IncidentDAOImpl(dataSource);
	}

	@Test
	void testGetAllUser() {
		List<User> listUser = dao.getAllUser();

		assertTrue(!listUser.isEmpty());

	}

	@Test
	void testGetIncident() {
		List<Incident> listIncident = dao.getIncident("0", "0", "0", "0", "0", "0", null);
		assertTrue(!listIncident.isEmpty());
	}

	@Test
	void testCreateIncident() {
		Incident incident = new Incident(0, 2, "Hoang", 2, null, 1, "no title", 1, "no description", 1, 2, 2, null,
				null, "");
		int result = dao.createIncident(incident);
		
		assertTrue(result > 0);
	}

	@Test
	void testUpdateStatusIncident() {
		int incidentId=1;
		int statusId = 2;
		int result = dao.updateStatusIncident(incidentId, statusId);
		assertTrue(result > 0);
	}

	@Test
	void testUpdateIncident() {
		Incident incident = new Incident(1, 2, "Hoang", 2, null, 1, "no title", 1, "no description", 1, 2, 2, null,
				null, "");
		int result = dao.updateIncident(incident);
		
		assertTrue(result > 0);
	}

	@Test
	void testGetIncidentById() {
		int id = 1;
		Incident incident = dao.getIncidentById(id);

		assertNotNull(incident);
	}

	@Test
	void testGetAllStatus() {
		List<Status> listStatus = dao.getAllStatus();
		assertTrue(!listStatus.isEmpty());
	}

	@Test
	void testGetAllOrganization() {
		List<Organization> listOrganization = dao.getAllOrganization();
		assertTrue(!listOrganization.isEmpty());
	}

	@Test
	void testGetAllRequestor() {
		List<Requestor> list = dao.getAllRequestor();
		assertTrue(!list.isEmpty());
	}

	@Test
	void testGetAllOrigin() {
		List<Origin> list = dao.getAllOrigin();
		assertTrue(!list.isEmpty());
	}

	@Test
	void testGetAllImpact() {
		List<Impact> list = dao.getAllImpact();
		assertTrue(!list.isEmpty());
	}

	@Test
	void testGetAllUrgency() {
		List<Urgency> list = dao.getAllUrgency();
		assertTrue(!list.isEmpty());
	}

	@Test
	void testGetAllPriority() {
		List<Priority> list = dao.getAllPriority();
		assertTrue(!list.isEmpty());
	}

	@Test
	void testGetAllType() {
		List<Type> list = dao.getAllType();
		assertTrue(!list.isEmpty());
	}

}
